// Desktop Scroller.
// Copyright (C) 2011-2012 Marcos Diaz <diazmencia@gmail.com>.
//
// Desktop Scroller is libre software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or newer.
//
// You should have received a copy of the GNU General Public License along with
// this file. If not, see <http://www.gnu.org/licenses/>.

const St = imports.gi.St
const Main = imports.ui.main
const WSP = imports.ui.workspaceSwitcherPopup
const Meta  = imports.gi.Meta


/* Main class for the extension. */
function DesktopScroller(){
	this.enable = function(){
		var monitor = Main.layoutManager.primaryMonitor
		var width = 1
		var height = monitor.height - 100
		var x = monitor.width - width
		var y = 30
		this.actor = new St.Button({style_class:'desktopscroller'})
		this.actor.set_position(x,y)
		this.actor.set_width(width)
		this.actor.set_height(height)
		this.actor.opacity = 0
		this.actor.connect('scroll-event', this.scroll.bind(this))
		Main.layoutManager.addChrome(this.actor)
		this.configure_overlay()
		this.wsp = null
	}
	this.disable = function(){
		Main.layoutManager.removeChrome(this.actor)
		this.actor.destroy()
		this.overview.disconnect(this.cid0)
		this.overview.disconnect(this.cid1)
	}
	this.scroll = function(actor, event){
		var direction = event.get_scroll_direction()
		if(direction==0) this.switch_workspace(-1)
		if(direction==1) this.switch_workspace(1)
	}
	this.switch_workspace = function(incremental){
		var index = global.screen.get_active_workspace_index()
		index += incremental
		global.screen.get_workspace_by_index(index).activate(false)
		this.display_wsp(incremental, index)
		Main.uiGroup.set_child_above_sibling(this.actor, null)
	}
	this.display_wsp = function(incremental, index){
		var direction = null
		if(incremental==-1) direction = Meta.MotionDirection.UP
		if(incremental==1) direction = Meta.MotionDirection.DOWN
		if(!this.wsp || this.wsp.actor.get_parent()==null){
			this.wsp = new WSP.WorkspaceSwitcherPopup()
		}
		this.wsp.display(direction, index)
	}
	this.configure_overlay = function(){
		this.overview = get_actor_by_name(global.overlay_group, 'overview')
		this.cid0 = this.overview.connect('key-focus-in',this.hide.bind(this))
		this.cid1 = this.overview.connect('key-focus-out',this.show.bind(this))
	}
	this.show = function(){
		this.actor.show()
	}
	this.hide = function(){
		this.actor.hide()
	}
}

/* Gnome-shell extension API. */
var desktopscroller = null
function init() {desktopscroller = new DesktopScroller()}
function enable() {desktopscroller.enable()}
function disable() {desktopscroller.disable()}

/* Other utils. */
function get_actor_by_name(actor, name){
	var children = actor.get_children()
	for(var i in children) if(children[i].name == name) return children[i]
	return null
}
